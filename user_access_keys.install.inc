<?php
/**
 * @file
 * API functions for *.install files to manage profile.module fields.
 *
 * @todo Implement an example install file.
 */


/**
 * A helper function for implementations of hook_requirements().
 *
 * Checks the profile.module field has not been removed or renamed.
 *
 * @param $phase
 *    The $phase parameter of hook_requirements().
 * @param &$reqs Array
 *    The array which the implementaion returns.  New requirements will be added.
 * @param $field String
 *    The profile.module field of which to check the requirements.
 */
function user_access_keys_field_check_requirements($phase, &$reqs, $field) {
  if ('runtime' == $phase) {
    $implementation = user_access_keys_implementations($field);
    $key = "{$field}_field";

    // Prepare t() and variables for it.
    $vars = array(
      '%name' => $implementation->name,
      '!field' => $implementation->field,
      '!url' => url('admin/user/profile/add/textfield'),
    );

    // Check that the profile.module field has not been deleted or renamed.
    $reqs[$key] = array(
      'title' => t('%name field', $vars),
      'value' => t('Exists'),
      'severity' => REQUIREMENT_OK,
    );
    if (!user_access_keys_field_exists($field)) {
      $reqs[$key]['value'] = t('Missing');
      $reqs[$key]['description'] = t('The %name profile field (<code>!field</code>) does not exist.  <a href="!url">Create the <code>!field</code> profile field now</a>.', $vars);
      $reqs[$key]['severity'] = REQUIREMENT_ERROR;
    }
  }
}


/**
 * A helper function for implementations of hook_enable().
 *
 * Creates the profile.module field if it does not already exist.
 *
 * @param $field String
 *    The profile.module field of which to check the requirements.
 */
function user_access_keys_field_enable($field) {
  // Do nothing if the profile.module field already exists.
  if (user_access_keys_field_exists($field)) {
    return;
  }

  $implementation = user_access_keys_implementations($field);

  // Create a new profile.module field to store each user's access key.
  $form_state = array(
    'values' => array(
      'category' => t('Third party services'),
      'title' => $implementation->name,
      'name' => $implementation->field,
      'explanation' => $implementation->instructions,
      'visibility' => '1',
      'required' => TRUE,
    ),
  );

  // `drush enable` does not bootstrap as far as update.php.
  module_load_include('admin.inc', 'profile');
  drupal_execute('profile_field_form', $form_state, 'textfield');
}

/**
 * A helper function for implementations of hook_uninstall().
 *
 * Deletes the profile.module field and removes all it's data.
 *
 * @param $field String
 *    The profile.module field of which to check the requirements.
 */
function user_access_keys_field_uninstall($field) {
  // Delete the profile.module field and it's data.
  // Get the FID of the profile.module field.
  $sql = 'SELECT fid FROM {profile_fields} WHERE name = "%s"';
  if ($fid = db_result(db_query($sql, $field))) {

    // A Form API form.
    $form_state = array('values' => array('confirm' => TRUE));

    // `drush enable` does not bootstrap as far as update.php.
    module_load_include('admin.inc', 'profile');
    drupal_execute('profile_field_delete', $form_state, $fid);
  }
}

/**
 * Checks whether the profile.module field still exists.
 *
 * @param $check_title String
 *    Checks also for profile fields with title of $check_title.
 * @return Boolean
 *    TRUE if the field exists, otherwise FALSE.
 */
function user_access_keys_field_exists($field) {
  $sql = 'SELECT * FROM {profile_fields} WHERE name = "%s" LIMIT 1';
  return (Boolean) db_result(db_query($sql, $field));
}
